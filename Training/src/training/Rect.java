/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package training;

/**
 *
 * @author DL9
 */
public class Rect {
    private String tp;
    private int x;
    private int y;
    private int w;
    private int h;
    Rect(String type,int x,int y,int w,int h)
    {
        type=type;
        x=x;
        y=y;
        w=w;
        h=h;
    }
    public void setX(int x)
    {
        x=x;
    }
    public void setY(int y)
    {
        y=y;
    }
    public void setW(int w)
    {
        w=w;
    }
    public void setH(int h)
    {
        h=h;
    }
    public void setType(String t)
    {
        tp=t;
    }
     public int getX()
    {
        return x;
    }
    public int getY()
    {
        return y;
    }
    public int getW()
    {
        return w;
    }
    public int getH()
    {
        return h;
    }
    public String getType()
    {
        return tp;
    }
}
    

